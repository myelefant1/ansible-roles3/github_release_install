## Github release installer

Downlods, installs and starts as systemd service an artifact from github

Tested on debian 10.

## Role parameters

| name                      | type   | optionnal | default value                                              | description                              |
| --------------------------|--------|-----------|------------------------------------------------------------|------------------------------------------|
| artifact_repo_owner       | string | no  |                                                                  | owner of the github repo |
| artifact_repo_name        | string | no  |                                                                  | name of the github repo |
| artifact_start_arguments  | string | no  |                                                                  | arguments appended to the command line |
| artifact_download_url     | string | yes | "https://github.com/{{ artifact_repo_owner }}/{{ artifact_repo_name }}/releases/download/v{{ _artifact_version }}/{{ artifact_asset_name }}"| URL for the archive download |
| artifact_description      | string | yes | "Service for {{ artifact_repo_owner }}/{{ artifact_repo_name }}" | decription of the systemd service |
| artifact_bin_name         | string | yes | "{{ artifact_repo_name }}"                                       | basename of the binary wich will be installed under /usr/local/bin/ |
| artifact_service_name     | string | yes | "{{ artifact_bin_name }}"                                        | Name of the systemd service |
| artifact_user             | string | yes | "{{ artifact_bin_name }}"                                        | User name of the service |
| artifact_group            | string | yes | "{{ artifact_user }}"                                            | User group of the systemd service |
| artifact_syslog_dentifier | string | yes | "{{ artifact_bin_name }}"                                        | Syslog identifier of the systemd service |
| artifact_start_arguments  | string | yes | ""                                                               | arguments appended to the execstart line of the systemd service |
| artifact_asset_platform   | string | yes | "linux-amd64"                                                    | Platform name of the asset file |
| artifact_asset_name       | string | yes | "{{ artifact_repo_name }}-{{ _artifact_version }}.{{ artifact_asset_platform }}.tar.gz"| artifact filename to be downloaded |
| artifact_unpack_file      | string | yes | "{{ artifact_repo_name }}-{{ _artifact_version }}.{{ artifact_asset_platform }}/{{ artifact_repo_name }}"| the file name which will be extracted and deployed |
| artifact_service_options  | array  | yes | []                                                               | list of additional lines added to the [Service] part of the systemd file |
| artifact_create_user_and_group | booleans  | yes | yes                                                      | setting to no disable user and group creation by this role |
| artifact_version          | string | yes | latest found on github                                           | The version which will be installed by this role|
| artifact_service_state    | string | yes | started                                                          | Expected state of the sevrice at the end of the run|

## Using this role

### ansible galaxy

Add the following in your *requirements.yml*.

```yaml
---
- src: https://gitlab.com/myelefant1/ansible-roles3/github_release_install.git
  scm: git
  version: v1.0
```

### Sample playbook

```yaml
- hosts: all
  roles:
    - role: github_release_install
      artifact_repo_owner: prometheus
      artifact_repo_name: haproxy_exporter
      artifact_start_arguments: '--haproxy.scrape-uri="http://localhost:5000/baz?stats;csv"'
- hosts: all
  roles:
    - role: github_release_install
      artifact_repo_owner: 'ivx'
      artifact_repo_name: 'yet-another-cloudwatch-exporter'
      artifact_version: '0.26.3-alpha'
      artifact_asset_name: "{{ artifact_repo_name }}_{{ artifact_version }}_{{ artifact_asset_platform }}.tar.gz"
      artifact_asset_platform: "Linux_x86_64"
      artifact_unpack_file: "yace"
```

## Tests

[tests/tests_prometheus_haproxy_exporter](tests/tests_prometheus_haproxy_exporter)
